import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { AppComponent } from './app.component';
import { HomeComponent } from './home/home.component';
import { RouterModule, Routes } from '@angular/router';

import { InputText  } from 'primeng/primeng';

const appRoutes: Routes = [
  { path: '', component: HomeComponent,  pathMatch: 'full'}
]

@NgModule({
  declarations: [
    AppComponent,
    HomeComponent
  ],
  imports: [
    FormsModule,
    BrowserModule,
        RouterModule.forRoot(appRoutes, {useHash: true})
    
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
