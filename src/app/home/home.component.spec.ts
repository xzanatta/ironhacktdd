import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import {  NgModule  } from '@angular/core';
import { HomeComponent } from './home.component';
import { FormsModule } from '@angular/forms';

describe('HomeComponent', () => {
  let component: HomeComponent;
  let fixture: ComponentFixture<HomeComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ HomeComponent ],
      imports: [ FormsModule ],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(HomeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it(`Calculate under the threshold`, async(() => {
    const home = fixture.debugElement.componentInstance;
    home.income =  10000;
    home.calculateTaxes();
    expect(home.netYearlySalary).toBe(8000);
  }));

  it(`Calculate over  the threshold`, async(() => {
    const home = fixture.debugElement.componentInstance;
    home.income =  100000;
    home.calculateTaxes();
    expect(home.netYearlySalary).toBe(60000);
  }));

  it(`Calculate under the threshold in 12 pays`, async(() => {
    const home = fixture.debugElement.componentInstance;
    home.income =  15000;
    home.numberOfPays = 12;
    home.calculateTaxes();
    expect(home.netMonthlySalary).toBe(1000);
  }));

});

  