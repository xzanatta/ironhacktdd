import { Component, OnInit } from '@angular/core';
import {  NgModule  } from '@angular/core';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {
  private income: number;
  private netYearlySalary: number;
  private isCalculated: boolean;
  private numberOfPays: number; 
  private netMonthlySalary: number;
  private threshol: number = 70000;
  constructor() { }

  ngOnInit() {
  }

  calculateTaxes(){
    let taxRatio; 
    if(this.income > this.threshol ){
      taxRatio = 40; 
    }else{
      taxRatio = 20;
    }
    this.netYearlySalary = (100 - taxRatio) * this.income / 100;
    this.netMonthlySalary = this.netYearlySalary / this.numberOfPays;
    this.isCalculated = true;
  } 
}
