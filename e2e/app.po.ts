import { browser, by, element } from 'protractor';

export class HomePage {
  navigateTo() {
    return browser.get('/');
  }
  getGrossIncomeText() {
    return element(by.id('grossIncomeLabel')).getText();
  }
  setIncome(value:number) {
    element(by.id('income')).sendKeys(value);
  }
  getNetIncomeText(){
    element(by.id("calculateButton")).click();
    return element(by.id('yearlyResult')).getText();
  }
}
