import { HomePage } from './app.po';

describe('ironhack App', () => {
  let page: HomePage;

  beforeEach(() => {
    page = new HomePage();
    page.navigateTo();
  });

  it('should display Gross Income', () => {
    expect(page.getGrossIncomeText()).toEqual('Gross Income');
  });
  
  it('should display a correct net value', () => {
    page.setIncome(15000);
    expect(page.getNetIncomeText()).toEqual('12000');
  });
});
